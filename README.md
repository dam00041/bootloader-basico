# ProgramHardSO 1.0
Un sencillo bootloader con funcionalidad muy básico implementado todo con interrupciones de la BIOS.

Realizado en ensamblador NASM,
y ejecutado a tráves de Qemu

## Interrupciones usadas y sus parámetros (INT)

10h Interrupción de video
> MOV AH, 02h Edita la posición del cursor

> MOV AH, 03h Selecciona el modo de video. También limpia la pantalla

> MOV AH, 0Ah Escribe un caracter en la posición del cursor

> MOV AH, 0Eh Escribe una cantidad de caracteres en pantalla

12h Para leer la memoria disponible hasta 640 KiB

16h Para controlar el teclado

19h Para reiniciar el sistema

1Ah Para hacer E/S del reloj


# Herramientas
> En la distribución de Ubuntu/Debian o derivados: <br>
> <code>sudo apt-get install qemu-x86 nasm</code>

# Ejecución
Simplemente necesitas Bash y ejecutar el archivo `ejecutar.sh`.

Esto genera la el binario en formato bin y luego
para poder ejecutarlo con qemu, crea una imagen de disquete para ejecutarla.

# Uso

![Inicio del bootloader](Imagenes/1 Antes.png)

`Ctrl` + `L` Limpia la pantalla

![Limpieza de pantalla](Imagenes/2 crtl-l.png)

`Ctrl` + `R` Reinicia el bootloader, es muy rapido y puede no notarse

![Reinicio del bootloader](Imagenes/1 Antes.png)

`Ctrl` + `T` Muestra la hora y minutos actual transformando el BCD en ASCII

![Inicio del bootloader](Imagenes/4 crtl-t.png)

`Ctrl` + `M` (No funciona) muestra la memoria disponible hasta 640 KiB

`Intro` Inserta una nueva linea

![Insertar una nueva linea](Imagenes/5 Intro.png)

`Retroceso` Elimina un caracter en pantalla

![Elimina un caracter en pantalla y actualiza su posición](Imagenes/6 Retroceso.png)

`Escape` Termina la ejecución del bootloader saliendo del bucle de teclado
