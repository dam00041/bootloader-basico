BITS_DE_RELLENO_FLOPPY equ 510
FIRMA_BOOTLOADER equ 0xAA55


POS_CARGA_BIOS_SO equ 7C00h

; Interrupciones (INT) de BIOS
; Provee servicios de video

BIOS_VIDEO equ 10h
	BIOS_VIDEO_MODO equ 03h		; Modo 80x25 a color 16 colores
	
	BIOS_VIDEO_POS_CURSOR equ 02h

	BIOS_VIDEO_TELETIPO equ 0Eh	; Salida de teletipo

; Interrupción del teclado en la BIOS
BIOS_TECLADO equ 16h

LIM_MAX_FIL_CUR equ 80
LIM_MIN_FIL_CUR equ 2

LIM_MAX_COL_CUR equ 25
LIM_MIN_COL_CUR equ 0

; Posición de memoria
POS_MEMORIA equ 600h

