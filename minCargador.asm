%include "definiciones.asm"

; Dirección donde la BIOS carga el código
ORG POS_CARGA_BIOS_SO

CLI
MOV AX, 0
MOV DS, AX
MOV ES, AX
CALL limpiarPantalla
MOV SI, tituloOS
MOV AH, BIOS_VIDEO_TELETIPO

bucle:
	LODSB		; Carga el siguiente char
	OR AL, AL	; Comprueba si es el final del string
	JZ continua
	INT BIOS_VIDEO
	JMP bucle
%include "teclado.asm"
	
	; No necesita ningun registro general (AX, BX, CX, DX)
continua:
	JMP teclado

tituloOS:
	db "ProgHardSO 1.0", 13, 10, 0

times BITS_DE_RELLENO_FLOPPY - ($ - $$) db 0	; rellena con ceros
dw FIRMA_BOOTLOADER					; Firma el bootloader


