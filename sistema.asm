limpiarPantalla:
	MOV AH, 00h
	MOV AL, BIOS_VIDEO_MODO
	INT BIOS_VIDEO
	RET

reiniciar:
	STI
	INT 19h
	RET

horaYMin:
	MOV AH, 02h
	INT 1Ah
	MOV AL, CH
	CALL mostrarPorPantalla
	CALL actualizaCursor
	MOV AL, 68h ; h
	CALL mostrarPorPantalla
	CALL actualizaCursor
	MOV AL, 20h
	CALL mostrarPorPantalla
	CALL actualizaCursor
	MOV AL, CL
	CALL mostrarPorPantalla
	CALL actualizaCursor
	MOV AL, 73 ; s
	CALL mostrarPorPantalla
	CALL actualizaCursor
	RET

memoriaDispo:
	STI
	INT 12h
	CMP AX, 0
	JE cero
	MOV BX, AX
	MOV AL, BH ; Parte alta de memoria
	CALL mostrarPorPantalla
	CALL actualizaCursor

	MOV AL, BL ; Parte baja de memoria
	CALL mostrarPorPantalla
	CALL actualizaCursor
	
	MOV AL, 6Bh ; Parte 
	CALL mostrarPorPantalla
	CALL actualizaCursor
	RET
	
cero:
	MOV AL, 30 ; Parte 
	CALL mostrarPorPantalla
	CALL actualizaCursor

	MOV AL, 6Bh ; Parte 
	CALL mostrarPorPantalla
	CALL actualizaCursor
	RET
	

; No necesita guardar ningún registro para otra función
; Funciona de forma correcta
guardarMemoria:
	MOV DX, POS_MEMORIA
	MOV DI, DX
	MOV DI, [DI]	; Se coge coloca DI en el sitio correcto

	STOSB		; Se guarda el nuevo byte en memoria
			; Se incrementa DI

	MOV CX, DI	; Guarda la posición
	MOV DI, DX	; Se coloca en la pos inicial
	MOV [DI], CX	; Se guarda el nuevo valor actualizado
	RET

; No necesita guardar ningún registro para otra función
; Funciona correctamente
reseteaMemoria:
	MOV DI, POS_MEMORIA
	MOV AX, 0
	MOV [DI], AX
	RET

comparacionComando:
	CMP AH, 26h
	JNE conCom2
	CMP AL, 0Ch
	JNE conCom2
	MOV CX, 1
	CALL limpiarPantalla
	CALL indicadorSistema
	JMP salComp
conCom2:
	CMP AH, 32h
	JNE conCom3
	CMP AL, 0Dh
	JNE conCom3
	MOV CX, 1
	CALL memoriaDispo
	CLI
	JMP salComp
conCom3:
	CMP AH, 14h
	JNE conCom4
	CMP AL, 14h
	JNE conCom4
	MOV CX, 1
	CALL horaYMin
	JMP salComp

conCom4:CMP AH, 13h
	JNE salComp
	CMP AL, 12h
	JNE salComp
	CALL reiniciar
salComp:
	RET
