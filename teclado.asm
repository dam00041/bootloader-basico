; Interrupciones BIOS relacionadas con el teclado en modo real

; 16h Control del teclado
;	0h
;		AH
;			Leer teclado
;		AL
;			Lee el boton del teclado (Codificación ASCII)
;			https://www.rapidtables.com/code/text/ascii-table.html

; 10h Provee servicios de video
;	01h Modfica el cursor de texto
;		
;	0ah
;		AH Escribir un caracter solo en la posición del cursor
;		CX Numero de veces que se va a imprimir el caracter

teclado:
	CALL indicadorSistema
	JMP tecladoBucle

posBorrado:
	CALL consigueInfoCursor

	MOV AH, BIOS_VIDEO_POS_CURSOR
	DEC DL
	INT BIOS_VIDEO

	MOV AL, 32 ; Tecla de Espacio
	CALL mostrarPorPantalla

	JMP tecladoBucle

; No necesita mantener los registros
nuevaLinea:
	CALL consigueInfoCursor

	MOV AH, BIOS_VIDEO_POS_CURSOR
	MOV DL, 0
	INC DH
	INT BIOS_VIDEO

	CALL indicadorSistema
	;CALL reseteaMemoria

; Necesita AH para iniciar la interrupción AL tiene el valor ASCII
tecladoBucle:
	MOV AH, 0h	; Leer teclado de la BIOS
	INT BIOS_TECLADO		
	CMP AL, 1Bh	; Tecla de escape
	JZ salir

	CMP AL, 0Dh	; Tecla intro o Carriage Return CR
	JZ nuevaLinea

	CMP AL, 08h	; Tecla de retroceso
	JZ posBorrado

	CALL comparacionComando
	CMP CX, 1
	JE tecladoBucle
	CALL mostrarPorPantalla

	CALL actualizaCursor
	
	
	;CALL guardarMemoria


	JMP tecladoBucle

consigueInfoCursor:
	MOV AH, 03h
	INT BIOS_VIDEO
	ret

mostrarPorPantalla:
	MOV AH, 0ah	; Mostrar por pantalla
	MOV CX, 1
	INT BIOS_VIDEO
	ret



actualizaCursor:
	CALL consigueInfoCursor
	MOV AH, BIOS_VIDEO_POS_CURSOR
	INC DL
	INT BIOS_VIDEO
	ret

indicadorSistema:
	MOV AL, 36		; Simbolo $
	call mostrarPorPantalla
	call consigueInfoCursor
	MOV AH, 02h
	MOV DL, 2
	INT BIOS_VIDEO
	ret

%include "sistema.asm"

salir: 
	HLT
